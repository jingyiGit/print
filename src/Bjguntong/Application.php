<?php
// 美达罗捷打印
// http://www.bjguntong.com/files/manual.pdf

namespace JyPrint\Bjguntong;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'https://api.bjguntong.com';
    protected $printName = '美达罗捷';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     *
     * @param $param
     * @return bool
     */
    public function add($param)
    {
        $param['wifiSn'] = $param['sn'];
        unset($param['sn']);
        $param['type'] = isset($param['type']) ? $param['type'] : 1;
        $params        = $this->getCommonParam($param);
        $res           = Http::httpPost($this->requestUrl . '/printer/add', $params);
        if ($res['code'] == 200 || ($res['code'] == 400 && strpos($res['msg'], '打印机已经添加') !== false)) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 删除打印机
     *
     * @param $param
     * @return bool
     */
    public function del($param)
    {
        $param['macSn'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/printer/delete', $params);
        if ($res['code'] == 200) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($param)
    {
        $param['macSn'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/emptyprintqueue', $params);
        if ($res['errNum'] == 0 && isset($res['retData'])) {
            return $res['retData'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 0=离线，1=wifi在线，3=4G在线，5打印机wifi和4G在线
     */
    public function getStatus($param)
    {
        $param          = $this->onlyfields($param, ['sn']);
        $param['macSn'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpGet($this->requestUrl . '/printer/status', $params);
        if ($res['code'] == 200 && isset($res['data'])) {
            return intval($res['data']['status']);
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 0=离线，1=在线，2=缺纸
     * https://www.kancloud.cn/fage/us_api/1342979
     */
    public function getAllStatus()
    {
        $params = $this->getCommonParam();
        $res    = Http::httpGet($this->requestUrl . '/printer/tenantStatus', $params);
        if ($res['code'] == 200 && isset($res['data'])) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $param['macSn'] = $param['sn'];
        unset($param['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/order/print', $params);
        if ($res['code'] == 200) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($params = [])
    {
        $time         = time() . rand(100, 999);
        $data         = array_merge([
            'mobilePhone' => $this->config['mobilePhone'],
            'timestamp'   => $time,
        ], $params);
        $data['sign'] = $this->getSign($time);
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($time)
    {
        return md5($this->config['mobilePhone'] . $this->config['ApiKey'] . $time);
    }
}
