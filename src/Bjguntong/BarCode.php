<?php

namespace JyPrint\Bjguntong;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Bjguntong',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => 'BR',
            'br_closure'  => true,
            'package'     => 'content',
            'lineAndBr'   => false,
            'cut'         => false,
            'cashBox'     => false,
            'barCode1'    => '<C><CODE128>{value}</CODE128></C>',
            'qrCode'      => '<C><QRCODE>{value}</QRCODE></C>',
            'left'        => '<BR>{value}</BR>',
            'center'      => '<BR><C>{value}</C></BR>',
            'right'       => false,
            'font_h1'     => '<B>{value}</B>',
            'font_h2'     => '<B>{value}</B>',
            'font_height' => '<L>{value}</L>',
            'font_width'  => '<W>{value}</W>',
            'font_bold'   => '<BOLD>{value}</BOLD>',
            'font_big'    => '<B>{value}</B>',
            'leftRight'   => false, // 左右对齐
            'row2col'     => false, // 1行2列
            'row3col'     => false, // 1行3列
            'row4col'     => false, // 1行4列
        ];
        return new UnifyPrint($config, $paper);
    }
}
