<?php
// 优声云打印
// https://www.kancloud.cn/fage/us_api/1342975

namespace JyPrint\Ushengyun;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'https://open-api.ushengyun.com';
    protected $printName = '优声云';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     *
     * @param array $param
     * @return bool
     */
    public function add($param)
    {
        $param['content'] = '绑定打印机成功<RN><RN>';
        if ($this->printTicket($param)) {
            return true;
        }
        return false;
    }
    
    /**
     * 通过wifiMac取出sn和密钥
     *
     * @param string $wifiMac 如：2c-f4-32-cd-dc-fb-00-00
     * @return false|mixed
     */
    public function getInfo($wifiMac)
    {
        $params = $this->getCommonParam(['sn' => $wifiMac]);
        $res    = Http::httpPostJson($this->requestUrl . '/printer/info', $params);
        if ($res['errNum'] == 0 && isset($res['retData'])) {
            return $res['retData'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($param)
    {
        $param['deviceid']     = $param['sn'];
        $param['devicesecret'] = $param['key'];
        unset($param['sn'], $param['key']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/printer/emptyprintqueue', $params);
        if ($res['errNum'] == 0 && isset($res['retData'])) {
            return $res['retData'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取打印机状态
     * 0=离线，1=在线，2=缺纸
     */
    public function getStatus($param)
    {
        if (!isset($param['key'])) {
            $this->setError('key不能为空');
            return false;
        }
        $param                 = $this->onlyfields($param, ['sn', 'key']);
        $param['deviceid']     = $param['sn'];
        $param['devicesecret'] = $param['key'];
        unset($param['sn'], $param['key']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/printer/status', $params);
        if ($res['errNum'] == 0 && isset($res['retData'])) {
            return (int)$res['retData']['status'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 设置设备音量
     *
     * @param array $param
     * @return bool|false
     */
    public function setVolume($param)
    {
        $param['deviceid']     = $param['sn'];
        $param['devicesecret'] = $param['key'];
        unset($param['sn'], $param['key']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/printer/sound', $params);
        if ($res['errNum'] == 0 && isset($res['retData']) && $res['retData']['status'] == 1) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $param['deviceid']     = $param['sn'];
        $param['devicesecret'] = $param['key'];
        $param['printdata']    = $param['content'];
        unset($param['content'], $param['sn'], $param['key']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/printer/print', $params);
        if ($res['errNum'] == 0) {
            return $res['retData'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($params)
    {
        $data         = array_merge([
            'appid'     => $this->config['appid'],
            'sound'     => 3,
            'timestamp' => time(),
        ], $params);
        $data['sign'] = $this->getSign($data);
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        if ($data == null) {
            return null;
        }
        ksort($data);
        $result_str = "";
        foreach ($data as $key => $val) {
            if ($key != null && $key != "" && $key != "sign") {
                $result_str = $result_str . $key . $val;
            }
        }
        return md5($result_str . $this->config['secret']);
    }
}
