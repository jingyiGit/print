<?php

namespace JyPrint\Wending;

use JyPrint\UnifyPrint\UnifyPrint;

/**
 * 小票
 */
trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Wending',
            'model'       => 'labelGroup',
            'labelName'   => false,
            'text'        => '<h3-s bold="false" justify="left">{value}</h3-s>',
            'br'          => '<br/>',
            'lineAndEach' => true, // 每个处理后面自动加上br(换行)
            'lineAndBr'   => false,
            'cut'         => '<cut/>',
            'cashBox'     => '', // 钱箱/声光报警器
            'barCode1'    => '<barcode value="{value}"/>',
            'barCode2'    => '<barcode value="{value}"/>',
            'qrCode'      => '<qrcode value="{value}"/>',
            'font_h1'     => '<h3 bold="true" justify="center">{value}</h3>',
            'font_h2'     => '<h3 bold="false" justify="center">{value}</h3>',
            'left'        => [
                'name'  => 'justify',
                'value' => 'left',
            ],
            'center'      => [
                'name'  => 'justify',
                'value' => 'center',
            ],
            'right'       => [
                'name'  => 'justify',
                'value' => 'right',
            ],
            'font_bold'   => [
                'name'  => 'bold',
                'value' => true,
            ],
            'font_big'    => '<h3>{value}</h3>',
            'leftRight'   => false,
            'row2col'     => '<tr2><th>{content1}</th><th>{content2}</th></tr2>',
            'row3col'     => '<tr3><th><h3-s>{content1}</h3-s></th><th><h3-s>{content2}</h3-s></th><th><h3-s>{content3}</h3-s></th></tr3>',
            'row4col'     => '<tr4><th><h3-s>{content1}</h3-s></th><th><h3-s>{content2}</h3-s></th><th><h3-s>{content3}</h3-s></th><th><h3-s>{content4}</h3-s></th></tr4>',
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
            
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
