<?php

namespace JyPrint\Poscom;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     * https://www.poscom.cn/index.php?id=152
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'     => 'Poscom',
            'model'     => 'labelGroup',
            'labelName' => 'gpWord',
            'text'      => "<gpWord Align=0 Bold=0 Wsize=0 Hsize=0 Reverse=0 Underline=0>{value}</gpWord>", // 只有标签组才有
            'br'        => '<gpBr/>',
            'lineAndBr' => false,
            'cut'       => '<gpCut/>',
            'logo'      => '<gpLogo/>',
            'cashBox'   => '<gpCashBox/>',
            'img'       => '<gpImg Align="a">{value}</gpImg>',
            'barCode1'  => "<gpBarCode Align=1 Type=7 Width=2 Height=80 Position=2>{value}</gpBarCode>",
            'barCode2'  => "<gpBarCode Align=1 Type=6 Width=2 Height=80 Position=2>{value}</gpBarCode>",
            'qrCode'    => "<gpQRCode Align=1 Size=8 Error=L>{value}</gpQRCode>",
            'left'      => [
                'name'  => 'Align',
                'value' => 0,
            ],
            'center'    => [
                'name'  => 'Align',
                'value' => 1,
            ],
            'right'     => [
                'name'  => 'Align',
                'value' => 2,
            ],
            'font_h1'   => "<gpWord Align=1 Bold=0 Wsize=1 Hsize=1 Reverse=0 Underline=0>{value}</gpWord>",
            'font_h2'   => "<gpWord Align=1 Bold=1 Wsize=0 Hsize=1 Reverse=0 Underline=0>{value}</gpWord>",
            
            'font_height' => [
                'name'  => 'Hsize',
                'value' => 1,
            ],
            'font_width'  => [
                'name'  => 'Wsize',
                'value' => 1,
            ],
            'font_bold'   => [
                'name'  => 'Bold',
                'value' => 1,
            ],
            'font_big'    => [
                [
                    'name'  => 'Hsize',
                    'value' => 1,
                ],
                [
                    'name'  => 'Wsize',
                    'value' => 1,
                ],
            ],
            'leftRight'   => false,
            // 左右对齐
            'row2col'     => "<gpTR2 Type=0><td>{content1}</td><td>{content2}</td></gpTR2>",
            // 1行2列
            'row3col'     => "<gpTR3 Type=0><td>{content1}</td><td>{content2}</td><td>{content3}</td></gpTR3>",
            // 1行3列
            'row4col'     => "<gpTR4 Type=0><td>{content1}</td><td>{content2}</td><td>{content3}</td><td>{content4}</td></gpTR4>",
            // 1行4列
        ];
        return new UnifyPrint($config, $paper);
    }
}
