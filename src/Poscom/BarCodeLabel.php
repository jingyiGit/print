<?php

namespace JyPrint\Poscom;

use JyPrint\UnifyPrintLabel\UnifyPrintLabel;

/**
 * 标签
 */
trait BarCodeLabel
{
    public function initPrintContentLabel()
    {
        $config = [
            'owner'       => 'Poscom',
            'scale'       => 8,    // 比例，如：1mm=8dots，则填8
            'row_spacing' => 5,    // 行间距
            'margin'      => 10,   // 边距
            
            'normal_font_width'  => 24,    // 正常的字体宽度
            'normal_font_height' => 24,    // 正常的字体高度
            
            'h1_font_width'           => 48,        // 加宽的字体宽度
            'h1_font_height'          => 48,        // 加高的字体高度
            
            // 基础文本
            'basics_text'             => 'TEXT {x},{y},"TSS24.BF2",0,{w},{h},"{value}"' . "\n",
            
            // 设置打印机设备内部的纸张大小
            'set_paper_size'          => "SIZE {width} mm,{height} mm\nGAP 2 mm,0 mm\nREFERENCE 0,0\nSPEED 2\nDENSITY 8\n",
            
            // 打印方向，正向出纸
            'print_direction_forward' => "DIRECTION 0\nSET HEAD ON\nSET PRINTKEY OFF\nSET KEY1 ON\nSET KEY2 ON\nSHIFT 0\nCLS\n",
            
            // 打印方向，反向出纸
            'print_direction_reverse' => "DIRECTION 1\nSET HEAD ON\nSET PRINTKEY OFF\nSET KEY1 ON\nSET KEY2 ON\nSHIFT 0\nCLS\n",
            
            // 最后的附加文本
            'after'                   => "PRINT 1,1\n",
        ];
        return new UnifyPrintLabel($config);
    }
}
