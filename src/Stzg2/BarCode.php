<?php

namespace JyPrint\Stzg2;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'        => 'Stzg2',
            'model'        => 'label',
            'labelName'    => false,
            'text'         => false,
            'br'           => '<BR>',
            'lineAndBr'    => false,
            'audio'        => '',
            'audio_refund' => '<AUDIO-CANCEL></AUDIO-CANCEL>',
            'cashBox'      => '<PLUGIN>',
            'logo'         => '<LOGO>',
            'cut'          => '<CUT>',
            'barCode1'     => '<BC128_A>{value}</BC128_A>',
            'barCode2'     => '<BC128_C>{value}</BC128_C>',
            'qrCode'       => '<C><QR>{value}</QR></C>',
            'left'         => '{value}<BR>',
            'center'       => '<C>{value}</C>',
            'right'        => '<RIGHT>{value}</RIGHT>',
            'font_h1'      => '<B>{value}</B>',
            'font_h2'      => '<B>{value}</B>',
            'font_height'  => '<L>{value}</L>',
            'font_width'   => '<W>{value}</W>',
            'font_bold'    => '<BOLD>{value}</BOLD>',
            'font_big'     => '<B>{value}</B>',
            'leftRight'    => false, // 左右对齐
            'row2col'      => false, // 1行2列
            'row3col'      => false, // 1行3列
            'row4col'      => false, // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'         => [
            
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
