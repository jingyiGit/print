<?php

namespace JyPrint\Stzg2;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * 水獭云打印
 *
 * @remark 使用打印接口
 * @remark 文档说明：http://docs.ijingyi.com/web/#/74/5136
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'https://print.csschina.com/f/Api/Open';
    protected $printName = '水獭掌柜2';
    private $access_token = '';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($apiname)
    {
        $data        = [
            'user'    => $this->config['user'],
            'stime'   => time(),
            'apiname' => $apiname,
        ];
        $data['sig'] = sha1($this->config['user'] . $this->config['user_key'] . $data['stime']);
        return $data;
    }
    
    /**
     * 添加打印机
     */
    public function add($param)
    {
        $data = $this->getCommonParam('Open_printerAddlist');
        
        // 添加打印机，打印机编号(必填) # 打印机型号(必填) # 备注名称(选填)
        // 多台打印机请换行（\n）
        $data['printerContent'] = "{$param['sn']}#{$param['key']}#{$param['name']}";
        $res                    = Http::httpPost($this->requestUrl, $data);
        if ($res['ret'] == 0 && (isset($res['data']['ok']) && count($res['data']['ok']) > 0)) {
            return true;
        }
        if (isset($res['data']['no']) && count($res['data']['no']) > 0) {
            return $this->setError($res['data']['no'][0]);
        }
        return $this->setError($res);
    }
    
    /**
     * 获取打印机状态
     * 0=离线，1=在线
     */
    public function getStatus($sn)
    {
        $data       = $this->getCommonParam('Open_queryPrinterStatus');
        $data['sn'] = $sn;
        $res        = Http::httpPost($this->requestUrl, $data);
        if ($res['ret'] == 0) {
            return $res['data'] == '离线' ? 0 : 1;
        }
        return $this->setError($res);
    }
    
    /**
     * 删除打印机
     *
     * @param array|string $sn 打印机编号
     */
    public function del($sn)
    {
        $data           = $this->getCommonParam('Open_printerDelList');
        $data['snlist'] = $sn;
        $res            = Http::httpPost($this->requestUrl, $data);
        if ($res['code'] == 0) {
            return true;
        }
        return $this->setError($res);
    }
    
    /**
     * 打印小票
     *
     * @param array $param 参数：sn & content
     */
    public function printTicket($param)
    {
        $data            = $this->getCommonParam('Open_printMsg');
        $data['sn']      = $param['sn'];
        $data['content'] = $param['content'];
        $data['times']   = $param['number'] > 0 ? $param['number'] : 1;
        $res             = Http::httpPost($this->requestUrl, $data);
        if ($res['ret'] == 0) {
            return $res['data'];
        } elseif ($res['ret'] == 1002) {
            return $this->setError('打印机未绑定 或 绑定被删除');
        }
        return $this->setError($res);
    }
}
