<?php

namespace JyPrint\A365;

use JyPrint\UnifyPrint\UnifyPrint;

/**
 * 365小票
 */
trait BarCode
{
    /**
     * 初始化统一打印
     * https://docs.ijingyi.com/web/#/74/2169
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'A365',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => '<BR>',
            'lineAndBr'   => false,
            'cut'         => '',
            'cashBox'     => '<PLUGIN>', // 钱箱/声光报警器
            'barCode1'    => '<C><BC128_A>{value}</BC128_A></C>',
            'barCode2'    => '<C><BC128_C>{value}</BC128_C></C>',
            'qrCode'      => '<C><QR>{value}</QR></C>',
            'left'        => '{value}<BR>',
            'center'      => '<C>{value}</C>',
            'right'       => '',
            'font_h1'     => '<B>{value}</B>',
            'font_h2'     => '<B>{value}</B>',
            'font_height' => '<L>{value}</L>',
            'font_width'  => '<W>{value}</W>',
            'font_bold'   => '{value}',
            'font_big'    => '<B>{value}</B>',
            'leftRight'   => false, // 左右对齐
            'row2col'     => false, // 1行2列
            'row3col'     => false, // 1行3列
            'row4col'     => false, // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
                'B',
                'L',
                'W',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
