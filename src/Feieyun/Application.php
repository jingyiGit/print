<?php
// 飞鹅
// http://www.feieyun.com/open/index.html

namespace JyPrint\Feieyun;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    use BarCodeLabel;
    
    private $config;
    private $requestUrl = 'http://api.feieyun.cn/Api/Open/';
    protected $printName = '飞鹅云';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     *
     * @param array $prints 打印机列表
     * @return bool
     */
    public function add($prints)
    {
        $createPrints = function ($prints) {
            $list = [];
            // 兼容单台的添加方式
            if (isset($prints['sn'])) {
                $prints = [$prints];
            }
            foreach ($prints as $print) {
                if (isset($print['name'])) {
                    $print['remark'] = $print['name'];
                }
                $list[] = "{$print['sn']}#{$print['key']}#{$print['remark']}#{$print['sim']}";
            }
            return implode("\n", $list);
        };
        $msgInfo      = [
            'printerContent' => $createPrints($prints),
        ];
        
        $msgInfo = $this->getCommonParam('Open_printerAddlist', $msgInfo);
        $res     = Http::httpPost($this->requestUrl, $msgInfo);
        if ($res['ret'] == 0 && $res['msg'] == 'ok' && count($res['data']['no']) == 0) {
            return true;
        } elseif (isset($res['data']['no']) && count($res['data']['no']) > 0) {
            if (strpos($res['data']['no'][0], '已被添加过）') !== false) {
                return true;
            } elseif (strpos($res['data']['no'][0], '一台打印机最多只能绑定 3 个账户') !== false) {
                $this->setError('飞鹅官方限制一台打印机最多只能绑定3个账户，请先解绑一个名额后再绑定！具体请咨询飞鹅官方客服。');
                return false;
            }
            $this->setError(count($res['data']['no']) == 1 ? $res['data']['no'][0] : $res['data']['no']);
            return false;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 删除打印机
     *
     * @param array|string $sns 打印机编号
     */
    public function del($sns)
    {
        if (is_array($sns)) {
            $sns = implode('-', $sns);
        }
        $param  = [
            'snlist' => $sns,
        ];
        $params = $this->getCommonParam('Open_printerDelList', $param);
        $res    = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok' && count($res['data']['no']) == 0) {
            return true;
        } elseif (isset($res['data']['no']) && count($res['data']['no']) > 0) {
            $this->setError(count($res['data']['no']) == 1 ? $res['data']['no'][0] : $res['data']['no']);
            return false;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($sn)
    {
        $param  = [
            'sn' => $sn,
        ];
        $params = $this->getCommonParam('Open_delPrinterSqs', $param);
        $res    = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 返回打印机状态信息。共三种：
     * 1、离线。
     * 2、在线，工作状态正常。
     * 3、在线，工作状态不正常。
     * 备注：异常一般是无纸，离线的判断是打印机与服务器失去联系超过2分钟。
     */
    public function getStatus($param)
    {
        $param  = $this->onlyfields($param, ['sn']);
        $params = $this->getCommonParam('Open_queryPrinterStatus', $param);
        $res    = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok') {
            if (isset($res['data']) && strpos($res['data'], '在线') !== false) {
                return 1;
            }
            return 0;
        }
        $this->setError($res);
        return 0;
    }
    
    /**
     * 查询指定打印机某天的订单统计数
     *
     * @param string $sn
     * @param null   $date
     * @return false|mixed
     */
    public function getOrderInfo($sn, $date = null)
    {
        $date   = $date ?: date('Y-m-d');
        $param  = [
            'sn'   => $sn,
            'date' => $date,
        ];
        $params = $this->getCommonParam('Open_queryOrderInfoByDate', $param);
        $res    = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok') {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $params['times'] = $param['number'] ?: 1;
        $params          = $this->getCommonParam('Open_printMsg', $param);
        $res             = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印标签
     */
    public function printLabel($param)
    {
        $params['times'] = $param['times'] ?: 1;
        $params          = $this->getCommonParam('Open_printLabelMsg', $param);
        // dd($this->requestUrl, $params);
        $res = Http::httpPost($this->requestUrl, $params);
        if ($res['ret'] == 0 && $res['msg'] == 'ok') {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($apiname, $params)
    {
        $time = time();
        return array_merge([
            'stime'   => $time,
            'user'    => $this->config['user'],
            'apiname' => $apiname,
            'sig'     => $this->getSign($time),
        ], $params);
    }
    
    /**
     * 生成签名
     */
    private function getSign($time)
    {
        return sha1($this->config['user'] . $this->config['ukey'] . $time);
    }
}
