<?php

namespace JyPrint\UnifyPrint;

trait Fun
{
    /**
     * 取文本长度，数字和字母x1，汉字x2
     */
    private function getTextLength($value = '')
    {
        if (!$value) {
            return 0;
        }
        $length = strlen($value);
        $temp1  = $this->getChineseCount($value);
        return $length - $temp1;
    }
    
    /**
     * 取汉字个数
     *
     * @param $value
     * @return float|int
     */
    private function getChineseCount($value)
    {
        $length = strlen($this->getChinese($value)) / 3;
        if (strpos($value, '【') !== false) {
            $length++;
        }
        if (strpos($value, '】') !== false) {
            $length++;
        }
        if (strpos($value, '，') !== false) {
            $length++;
        }
        if (strpos($value, '。') !== false) {
            $length++;
        }
        if (strpos($value, '：') !== false) {
            $length++;
        }
        if (strpos($value, '（') !== false) {
            $length++;
        }
        if (strpos($value, '）') !== false) {
            $length++;
        }
        if (strpos($value, '《') !== false) {
            $length++;
        }
        if (strpos($value, '》') !== false) {
            $length++;
        }
        if (strpos($value, '「') !== false) {
            $length++;
        }
        if (strpos($value, '」') !== false) {
            $length++;
        }
        return $length;
    }
    
    /**
     * 取出全部汉字
     *
     * @param string $value
     * @return string
     */
    private function getChinese($value)
    {
        preg_match_all('/[\x{4e00}-\x{9fa5}]+/u', $value, $res);
        return implode('', $res[0]);
    }
    
    /**
     * 取出全部字母数字
     *
     * @param string $value
     * @return string
     */
    private function getAlphanumeric($value)
    {
        preg_match_all('/\w+/', $value, $res);
        return implode('', $res[0]);
    }
    
    /**
     * 取出全部符号
     *
     * @param string $value
     * @return string
     */
    private function getSymbol($value)
    {
        preg_match_all('/[~!@#\$%^&\*\(\)\+\-_=,\.:\?<>]+/', $value, $res);
        return implode('', $res[0]);
    }
    
    /**
     * 文本截断
     *
     * @param string $string
     * @param string $length
     * @param string $dot
     * @return string
     */
    private function cutstr($string, $length, $dot = '')
    {
        if (strlen($string) <= $length) {
            return $string;
        }
        
        $pre    = chr(1);
        $end    = chr(1);
        $string = str_replace(['&amp;', '&quot;', '&lt;', '&gt;'], [
            $pre . '&' . $end,
            $pre . '"' . $end,
            $pre . '<' . $end,
            $pre . '>' . $end,
        ], $string);
        
        $strcut = '';
        
        $n = $tn = $noc = 0;
        while ($n < strlen($string)) {
            $t = ord($string[$n]);
            if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1;
                $n++;
                $noc++;
            } elseif (194 <= $t && $t <= 223) {
                $tn  = 2;
                $n   += 2;
                $noc += 2;
            } elseif (224 <= $t && $t <= 239) {
                $tn  = 3;
                $n   += 3;
                $noc += 2;
            } elseif (240 <= $t && $t <= 247) {
                $tn  = 4;
                $n   += 4;
                $noc += 2;
            } elseif (248 <= $t && $t <= 251) {
                $tn  = 5;
                $n   += 5;
                $noc += 2;
            } elseif ($t == 252 || $t == 253) {
                $tn  = 6;
                $n   += 6;
                $noc += 2;
            } else {
                $n++;
            }
            
            if ($noc >= $length) {
                break;
            }
            
        }
        if ($noc > $length) {
            $n -= $tn;
        }
        
        $strcut = substr($string, 0, $n);
        
        $strcut = str_replace([$pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end], [
            '&amp;',
            '&quot;',
            '&lt;',
            '&gt;',
        ], $strcut);
        
        $pos = strrpos($strcut, chr(1));
        if ($pos !== false) {
            $strcut = substr($strcut, 0, $pos);
        }
        return $strcut . $dot;
    }
    
    /**
     * 取数组深度(维度)
     *
     * @param $array
     * @return int
     */
    private function depth($array)
    {
        $max_depth = 1;
        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = self::depth($value) + 1;
                
                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }
        return $max_depth;
    }
}
