<?php
// 统计打印，小票类

namespace JyPrint\UnifyPrint;

class UnifyPrint
{
    use Fun;
    use Row;
    use Font;
    
    // 打印模式 http://docs.ijingyi.com/web/#/72/1953
    private $printModel = 'label';
    public $printFormat = [];  // 打印格式
    
    private $paperFormat = [    // 纸张格式
        'character'   => 32,
        'paper_width' => 58,
    ];
    protected string $printContent = '';
    protected $returnContent = false;
    
    /**
     * 设置统一打印格式
     *
     * @param array $format 打印格式
     * @param array $paper  纸张格式
     */
    public function __construct($format, $paper = [])
    {
        $this->printFormat = $format;
        $this->printModel  = isset($format['model']) ? $format['model'] : 'label';
        if ($paper) {
            $this->paperFormat = $paper;
        }
        if (!isset($this->printFormat['sort'])) {
            $this->printFormat['sort'] = false;
        }
    }
    
    /**
     * 设置打印格式参数
     *
     * @param string $key
     * @param string $value
     */
    public function setPrintFormat($key, $value)
    {
        $this->printFormat[$key] = $value;
    }
    
    /**
     * h1大标题
     *
     * @param string $value
     * @return void
     */
    public function h1($value, $align = 'center')
    {
        // 已有格式
        if ($this->printFormat['font_h1']) {
            $temp = str_replace('{value}', $value, $this->printFormat['font_h1']);
        } else {
            $temp = $this->getBaseText($temp);
        }
        $this->returnContent = true;
        if ($align == 'left') {
            $temp = $this->left($temp, false, false, false);
        } else if ($align == 'center') {
            $temp = $this->center($temp, false, false, false);
        } else if ($align == 'right') {
            $temp = $this->right($temp, false, false, false);
        }
        $this->printContent .= $temp . $this->lineAndEach();
    }
    
    /**
     * h2大标题
     *
     * @param string $value
     * @param string $align 对齐方式
     * @return void
     */
    public function h2($value, $align = 'center')
    {
        // 已有格式
        if ($this->printFormat['font_h2']) {
            $temp = str_replace('{value}', $value, $this->printFormat['font_h2']);
        } else {
            $temp = $this->getBaseText($temp);
        }
        
        if ($align == 'left') {
            $this->left($temp, false, false, false);
        } else if ($align == 'center') {
            $this->center($temp, false, false, false);
        } else if ($align == 'right') {
            $this->right($temp, false, false, false);
        }
    }
    
    /**
     * 常规文本
     *
     * @param string $value
     * @param false  $bold
     * @param false  $height
     * @param false  $width
     * @return array|mixed|string|string[]
     */
    public function text($value, $bold = false, $height = false, $width = false)
    {
        $temp = $this->getBaseText($value, $bold, $height, $width);
        
        // 换行符是否为闭包
        if (isset($this->printFormat['br_closure']) && $this->printFormat['br_closure']) {
            $temp               = "<{$this->printFormat['br']}>{$temp}</{$this->printFormat['br']}>";
            $this->printContent .= $temp . $this->lineAndEach();
            
        } else {
            $this->printContent .= $temp . $this->lineAndEach();
            if ($this->printFormat['model'] != 'labelGroup') {
                $this->br();
            }
        }
        return $temp;
    }
    
    /**
     * 居左打印
     *
     * @param string $value
     * @param bool   $bold   字体加粗
     * @param bool   $height 字体加高
     * @param bool   $width  字体加宽
     * @return string
     */
    public function left($value, $bold = false, $height = false, $width = false)
    {
        $temp = $this->getBaseText($value, $bold, $height, $width);
        $temp = $this->handleReturnContent('left', $temp);
        $temp = $this->sort($temp);
        
        // 只返回文本
        if ($this->returnContent) {
            $this->returnContent = false;
            return $temp;
        }
        
        $this->printContent .= $temp . $this->lineAndEach();
    }
    
    /**
     * 居中打印
     *
     * @param string $value
     * @param bool   $bold   字体加粗
     * @param bool   $height 字体加高
     * @param bool   $width  字体加宽
     * @return string
     */
    public function center($value, $bold = false, $height = false, $width = false)
    {
        $temp = $this->getBaseText($value, $bold, $height, $width);
        $temp = $this->handleReturnContent('center', $temp);
        
        $temp = $this->sort($temp);
        
        // 只返回文本
        if ($this->returnContent) {
            $this->returnContent = false;
            return $temp;
        }
        
        $this->printContent .= $temp . $this->lineAndEach();
    }
    
    /**
     * 居右打印
     *
     * @param string $value
     * @param bool   $bold   字体加粗
     * @param bool   $height 字体加高
     * @param bool   $width  字体加宽
     * @return string
     */
    public function right($value, $bold = false, $height = false, $width = false)
    {
        $temp = $this->getBaseText($value, $bold, $height, $width);
        
        // 未设置右居，或右居有问题(像美达罗捷)
        if (!isset($this->printFormat['right']) || !$this->printFormat['right']) {
            // $this->leftRight('', $temp);
            $this->rightAlign($temp, $width);
            
            // 打印机直接支持右对齐
        } else {
            $temp = $this->handleReturnContent('right', $temp);
            $temp = $this->sort($temp);
            
            // 只返回文本
            if ($this->returnContent) {
                $this->returnContent = false;
                return $temp;
            }
            $this->printContent .= $temp . $this->lineAndEach();
        }
    }
    
    /**
     * 右对齐打印
     *
     * @param string $value 要打印的文本
     * @param bool   $width 是否加宽
     * @return void
     */
    private function rightAlign($value, $width)
    {
        $length = $this->paperFormat['character'] ?? 32;
        
        // 字符长度
        $text_length = $this->getTextLength(strip_tags($value));
        if ($width || $this->isBig($value)) {
            $text_length = $text_length * 2;
        }
        
        $pad_length = $length - $text_length;
        if ($pad_length < 0) {
            $pad_length = 0;
        }
        
        $this->text(str_repeat(' ', $pad_length) . $value);
    }
    
    /**
     * 切刀/切纸
     *
     * @return void
     */
    public function cut()
    {
        if (isset($this->printFormat['cut'])) {
            $this->printContent .= $this->printFormat['cut'];
        }
    }
    
    /**
     * 换行
     *
     * @param int $count 换行数量
     * @return void
     */
    public function br($count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            // 换行符为闭包模式，如：<BR>我是文本</BR>
            if (isset($this->printFormat['br_closure']) && $this->printFormat['br_closure']) {
                $this->printContent .= "<{$this->printFormat['br']}></{$this->printFormat['br']}>";
            } else {
                $this->printContent .= $this->printFormat['br'];
            }
        }
    }
    
    /**
     * 添加分割线
     * 58mm的机器 32
     * 76mm的机器 33
     * 80mm的机器 48
     *
     * @param string $line
     * @param string $title
     * @return void
     */
    public function splitLine($line = null, $title = '', $fontBig = false)
    {
        /**
         * 创建横线，带标题
         *
         * @param string $title   标题
         * @param int    $length  长度(如：58mm的机器 32)
         * @param string $line    分割符
         * @param bool   $fontBig 字体是否加大
         */
        $splitLineUseTitle = function ($title, $length, $line, $fontBig = false) {
            if ($fontBig) {
                $titleLength = $this->getTextLength($title) * 2;
                $title       = $this->font_big($title);
            } else {
                $titleLength = $this->getTextLength($title);
            }
            // 取余数，(32 - 标题长度) % 2，取余，如果有余数，直接加到右侧
            $remainder = intval(($length - $titleLength) % 2);
            
            $length = intval(($length - $titleLength) / 2);
            return str_pad('', $length, $line) . $title . str_pad('', $length + $remainder, $line);
        };
        
        if (!$line) {
            if (isset($this->printFormat['line']) && $this->printFormat['line']) {
                $line = $this->printFormat['line'];
            } else {
                $line = '-';
            }
        }
        
        $length = isset($this->paperFormat['character']) ? $this->paperFormat['character'] : 32;
        
        // 分割线前面加换行符
        if (!isset($this->printFormat['lineAndBr']) || $this->printFormat['lineAndBr'] == true) {
            $this->br();
        }
        // 横线
        if ($title) {
            $value = $splitLineUseTitle($title, $length, $line, $fontBig);
        } else {
            $value = str_pad('', $length, $line);
        }
        
        // 标签组
        if ($this->printFormat['model'] == 'labelGroup') {
            $temp = $this->getBaseText($value);
        } else {
            $temp = $value;
        }
        
        // 换行符为闭包模式，如：<BR>我是文本</BR>
        if (isset($this->printFormat['br_closure']) && $this->printFormat['br_closure']) {
            $temp = "<{$this->printFormat['br']}>{$temp}</{$this->printFormat['br']}>";
        }
        
        $this->printContent .= $temp . $this->lineAndEach();
    }
    
    /**
     * 条码打印
     */
    public function barCode($value, $type = 'number')
    {
        if ($type == 'number') {
            $temp = isset($this->printFormat['barCode2']) ? $this->printFormat['barCode2'] : $this->printFormat['barCode1'];
        } else {
            $temp = $this->printFormat['barCode1'];
        }
        $this->printContent .= str_replace('{value}', $value, $temp) . $this->lineAndEach();
    }
    
    /**
     * 二维码打印
     */
    public function qrCode($value)
    {
        $this->printContent .= str_replace('{value}', $value, $this->printFormat['qrCode']) . $this->lineAndEach();
    }
    
    /**
     * 语音
     */
    public function audio($value = null)
    {
        if ($this->printFormat['audio']) {
            if ($value) {
                $this->printContent .= str_replace('{value}', $value, $this->printFormat['audio']) . $this->lineAndEach();
            } else {
                $this->printContent .= $this->printFormat['audio'] . $this->lineAndEach();
            }
        }
    }
    
    /**
     * 退款语音
     */
    public function audio_refund($value = null)
    {
        if ($this->printFormat['audio_refund']) {
            if ($value) {
                $this->printContent .= str_replace('{value}', $value, $this->printFormat['audio_refund']) . $this->lineAndEach();
            } else {
                $this->printContent .= $this->printFormat['audio_refund'] . $this->lineAndEach();
            }
        }
    }
    
    /**
     * 获取内容
     *
     * @param bool $debug
     * @return string
     */
    public function getPrintContent($debug = false)
    {
        $temp = $this->printContent;
        if (isset($this->printFormat['package']) && $this->printFormat['package']) {
            $temp = "<{$this->printFormat['package']}>" . $temp . "</{$this->printFormat['package']}>";
        }
        if ($debug) {
            return str_replace($this->printFormat['br'], "\n", $temp);
        } else {
            return $temp;
        }
    }
    
    /**
     * 处理返回内容
     *
     * @param string $field 字段名
     * @param string $value 值
     * @return array|string|string[]
     */
    private function handleReturnContent($field, $value)
    {
        if ($this->printModel == 'label') {
            return str_replace('{value}', $value, $this->printFormat[$field]);
            
        } else if ($this->printModel == 'labelGroup') {
            return $this->handleFontLabelGroup($value, $field);
            
        } else {
            return $value;
        }
    }
    
    /**
     * 获取基础文本
     *
     * @return mixed|string
     */
    private function getBaseText($value, $bold = false, $height = false, $width = false)
    {
        if ($this->printFormat['text']) {
            if (stripos($value, '<' . $this->printFormat['labelName']) === false) {
                $temp = str_replace('{value}', $value, $this->printFormat['text']);
            } else {
                $temp = $value;
            }
        } else {
            $temp = $value;
        }
        if ($width && $height) {
            $width && $temp = $this->font_big($temp);
        } else {
            $width && $temp = $this->font_width($temp);
            $height && $temp = $this->font_height($temp);
        }
        $bold && $temp = $this->font_bold($temp);
        return $temp;
    }
    
    /**
     * 进行排序
     * 从[外]到[里]的顺序，如：<C><S2>结帐单</S2></C>
     */
    public function sort($value)
    {
        $sort = $this->printFormat['sort'] ?: false;
        if (!$sort || !is_array($sort)) {
            return $value;
        }
        // 倒序一下
        krsort($sort);
        
        // print_r('sort 原始内容：' . htmlspecialchars($value));
        // echo "<br><br>";
        
        // 取出所有标签组，如：<B1><S1>宽高粗都加的文本</S1></B1>，返回 ['<B1>', '<S1>']
        $getLabel = function ($value) {
            if (!preg_match_all('/\<(\w*?)\>/', $value, $res)) {
                return [];
            }
            return $res[1];
        };
        
        // 出所有标签组
        $labels = $getLabel($value);
        
        // 去除标签，只取值，如：<B1><S1>宽高粗都加的文本</S1></B1>，最终只拿“宽高粗都加的文本”
        $original = strip_tags($value);
        
        // 换行符是否在标签内部，如：<C>{value}<BR></C>
        $br_inside = (isset($this->printFormat['br_inside']) && $this->printFormat['br_inside']) ?? false;
        
        // 是否存在换行符
        $br_exist = strpos($value, $this->printFormat['br']) !== false;
        
        // 排序
        foreach ($sort as $v) {
            if ($br_inside && $br_exist) {
                $br_inside = false;
                $original  = "{$original}{$this->printFormat['br']}";
            }
            if (in_array($v, $labels)) {
                $original = "<{$v}>{$original}</{$v}>";
            }
        }
        
        // 加上未在排序数组里的标签
        $br = str_replace(['<', '>'], '', $this->printFormat['br']);
        foreach ($labels as $v) {
            if (strpos($original, "<{$v}>") !== false) {
                continue;
            }
            
            // 是否换行符
            if ($br == $v) {
                $original = $original . $this->printFormat['br'];
            } else {
                $original = "<{$v}>{$original}</{$v}>";
            }
        }
        return $original;
    }
    
    /**
     * 每个内容后面都跟上br
     *
     * @return mixed|string
     */
    private function lineAndEach()
    {
        return (isset($this->printFormat['lineAndEach']) && $this->printFormat['lineAndEach']) ? $this->printFormat['br'] : '';
    }
}
