<?php

namespace JyPrint\Kernel;

class Response
{
    protected $error;
    protected $printName = '';
    protected $errorNum = [];
    
    public function fail($value, $status = 0)
    {
        exit(json_encode([
            'message' => $value,
            'status'  => $status,
        ]));
    }
    
    protected function setError($error)
    {
        if (isset($error['code']) && isset($this->errorNum[$error['code']])) {
            $error['msg'] = $this->errorNum[$error['code']];
        } else if (is_string($error)) {
            $error = ['msg' => $error];
        }
        $error['msg'] = $this->printName . ': ' . $error['msg'];
        $this->error  = $error;
        return false;
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * 生成uuid
     *
     * @param string $separate 分隔符
     * @return string
     */
    protected static function uuid($separate = '-')
    {
        $chars = md5(uniqid(mt_rand(), true));
        return substr($chars, 0, 8) . $separate
               . substr($chars, 8, 4) . $separate
               . substr($chars, 12, 4) . $separate
               . substr($chars, 16, 4) . $separate
               . substr($chars, 20, 12);
    }
    
    /**
     * 从数组中筛选特定字段(只获取特定的字段)
     *
     * @param array $data   源数组
     * @param mixed $fields 筛选字段
     * @param int   $level  处理级别
     * @return array
     */
    protected function onlyfields(&$data, $fields, $level = 1)
    {
        if ($level > 2) {
            return $data;
        }
        if ($fields == '*') {
            return $data;
        }
        if (!is_array($data)) {
            return $data;
        }
        if (is_string($fields)) {
            $fields = explode(',', $fields);
        }
        if (!is_array($fields)) {
            $fields = (array)$fields;
        }
        
        if (!is_assoc($data)) {
            foreach ($data as &$v) {
                onlyfields($v, $fields, $level + 1);
            }
            return $data;
        }
        
        foreach (array_keys($data) as $k) {
            $flag = false;
            foreach ($fields as $f) {
                if (fnmatch($f, $k)) {
                    $flag = true;
                    break;
                }
            }
            
            if (!$flag) {
                unset($data[$k]);
            }
        }
        return $data;
    }
}
