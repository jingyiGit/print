<?php
// 商鹏云打印
// http://www.spyun.net/open/index.html

namespace JyPrint\Spyun;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    
    private $config;
    private $requestUrl = 'https://open.spyun.net';
    protected $printName = '商鹏云';
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     */
    public function add($param)
    {
        $param['pkey'] = $param['key'];
        unset($param['key']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/v1/printer/add', $params);
        if ($res['errorcode'] == 0 || $res['errorcode'] == 5) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 删除打印机
     */
    public function del($sn)
    {
        $params = $this->getCommonParam(['sn' => $sn]);
        $res    = Http::httpDelete($this->requestUrl . '/v1/printer/delete', $params);
        if ($res['errorcode'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($sn)
    {
        $params = $this->getCommonParam(['sn' => $sn]);
        $res    = Http::httpDelete($this->requestUrl . '/v1/printer/cleansqs', $params);
        if ($res['errorcode'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 0：正常，1：异常
     */
    public function getStatus($param)
    {
        $param  = $this->onlyfields($param, ['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpGet($this->requestUrl . '/v1/printer/info', $params);
        if ($res['errorcode'] == 0) {
            return $res['online'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 修改打印机信息
     *
     * @param array $param
     * @return false|mixed
     */
    public function setPrintInfo($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPatch($this->requestUrl . '/v1/printer/update', $params);
        if ($res['errorcode'] == 0) {
            return $res;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 修改打印机配置参数
     *
     * @param array $param
     * @return false|mixed
     */
    public function setSetting($param)
    {
        if ($params['voice']) {
            $params['voice'] = strtoupper($params['voice']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPatch($this->requestUrl . '/v1/printer/setting', $params);
        if ($res['errorcode'] == 0) {
            return $res;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        if ($param['number']) {
            $param['times'] = $param['number'];
            unset($param['number']);
        }
        $params = $this->getCommonParam($param);
        $res    = Http::httpPost($this->requestUrl . '/v1/printer/print', $params);
        if ($res['errorcode'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($params)
    {
        $data         = array_merge([
            'appid'     => $this->config['appid'],
            'timestamp' => time(),
            'sn'        => $params['sn'],
        ], $params);
        $data['sign'] = $this->getSign($data);
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($data)
    {
        if ($data == null) {
            return null;
        }
        ksort($data);
        $result_str = "";
        foreach ($data as $key => $val) {
            if ($key != null && $key != "" && $key != "sign") {
                $result_str .= $key . '=' . $val . "&";
            }
        }
        return strtoupper(md5($result_str . "appsecret=" . $this->config['secret']));
    }
}
