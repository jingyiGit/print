<?php

namespace JyPrint\Spyun;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Spyun',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => '<BR>',
            'lineAndBr'   => false,
            'cut'         => '<CUT>',
            'barCode1'    => '<C><BC128_C>{value}</BC128_C></C>',
            'qrCode'      => '<C><QRCODE>{value}</QRCODE></C>',
            'left'        => '{value}<BR>',
            'center'      => '<C>{value}</C>',
            'right'       => '<R>{value}</R>',
            'font_h1'     => '<L1>{value}</L1>',
            'font_h2'     => '<L1>{value}</L1>',
            'font_height' => '<H>{value}</H>',
            'font_width'  => '<W>{value}</W>',
            'font_bold'   => '<B>{value}</B>',
            'font_big'    => '<L1>{value}</L1>',
            'leftRight'   => false, // 左右对齐
            'row2col'     => false, // 1行2列
            'row3col'     => false, // 1行3列
            'row4col'     => false, // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
                'L1',
                'H',
                'W',
                'B',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
