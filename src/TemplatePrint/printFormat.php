<?php
// 自定义信息，如：某个字段显示隐藏，字体大小

namespace JyPrint\TemplatePrint;

trait printFormat
{
  // 自定义信息，如：某个字段显示隐藏，字体大小
  private static $print_format = [
    // 商品信息
    'goodList'      => [
      'show'       => true,
      'fontBold'   => false,
      'fontHeight' => false,
    ],
    
    // 套餐信息
    'specsInfo'     => [
      'show'       => true,
      'fontBold'   => false,
      'fontHeight' => false,
    ],
    
    // 条形码
    'barCode'       => [
      'show' => true,
    ],
    
    // 虚拟号码
    'virtualNumber' => [
      'show'       => true,
      'fontBold'   => true,
      'fontHeight' => true,
    ],
    
    // 顾客姓名/电话
    'customer'      => [
      'show'          => true,
      'fontBold'      => false,
      'fontHeight'    => true,
      'fontWidth'     => true,
      'phoneShowType' => 0, // 0=显示全部，1中间4位打码，2只显示尾号(4位)
    ],
    
    // 收货地址
    'address'       => [
      'show'       => true,
      'fontBold'   => false,
      'fontHeight' => false,
      'fontWidth'  => false,
    ],
    
    // 骑手信息（姓名/电话）
    'rider'         => [
      'show'       => false,
      'fontBold'   => false,
      'fontHeight' => true,
      'fontWidth'  => false,
    ],
  ];
  
  /**
   * 取自定义信息字段
   *
   * @param string $key
   * @param string $default 如果为空，返回的默认值
   * @return array|mixed
   */
  private static function getDiyInfo($key, $default = '')
  {
    $list = explode('.', $key);
    $temp = self::$print_format;
    foreach ($list as $k) {
      if (isset($temp[$k])) {
        $temp = $temp[$k];
      } else {
        return $default;
      }
    }
    return $temp;
  }
  
  /**
   * 取指定字段是否要显示
   *
   * @param string $key
   * @return false|mixed
   */
  private static function getDiyInfoShow($key)
  {
    if ($temp = self::getDiyInfo($key, false)) {
      return $temp['show'];
    }
    return false;
  }
  
}
