<?php

namespace JyPrint\TemplatePrint;

trait Error
{
  public static function getError()
  {
    return self::$error;
  }
  
  protected static function setError($error)
  {
    self::$error = $error;
    return false;
  }
}
