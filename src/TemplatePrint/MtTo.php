<?php
// 美团，将美团订单转换成可直接打印的订单数据

namespace JyPrint\TemplatePrint;

/**
 * 美团
 * 内容格式说明：http://docs.ijingyi.com/web/#/74/2910
 */
trait MtTo
{
    /**
     * 针对饿了么订单，自动转换打印数据
     *
     * @param array $order      订单信息
     * @param array $extra_info 额外信息
     * @return array|false
     */
    public static function transformationMt($order, $extra_info = [])
    {
        if (!self::isMtOrder($order)) {
            return false;
        }
        
        // 替换美团备注上的一些没用信息
        // 收餐人隐私号 15532806034_0068，手机号 186****4400 环保单，顾客不需要附带餐具 到店自取
        $order['caution'] = preg_replace("#收餐人隐私号.*?\d{4}，#", '', $order['caution']);
        $order['caution'] = preg_replace("#手机号.*?\d{4}\s?#", '', $order['caution']);
        $order['caution'] = str_replace("环保单，", '', $order['caution']);
        
        $order_info = [
            'orderId'                => $order['orderId'],
            'daySeq'                 => $order['daySeq'],
            'Remark'                 => $order['caution'],
            'peopleNumber'           => $order['dinnersNumber'],
            'order_time'             => self::handleTime($order['ctime']),
            'deliveryTime'           => self::handleTime($order['deliveryTime']),
            'reserve_time'           => self::handleTime($order['deliveryTime']),
            
            // 价格相关
            'originalPrice'          => $order['originalPrice'], // 原价
            'total'                  => $order['total'],         // 总价(实际收入)
            
            // 收货信息
            'customerName'           => $order['recipientName'],
            'customerPhone'          => $order['recipientPhone'],
            'customerAddress'        => $order['recipientAddress'],
            'customerAddressDetails' => $order['recipientAddressDesensitization'],
            
            // 骑手信息
            'riderName'              => isset($order['logisticsDispatcherName']) ? $order['logisticsDispatcherName'] : '',
            'riderPhone'             => isset($order['logisticsDispatcherMobile']) ? $order['logisticsDispatcherMobile'] : '',
        ];
        
        // 商品列表
        $groups    = self::goodGroup($order);
        $good_list = [];
        foreach ($groups as $number => $goods) {
            if (count($groups) == 1) {
                $number = 1;
            } else {
                $number++;
            }
            $good_list[$number] = [
                'name'  => $number . "号篮子",
                'type'  => 'normal',
                'items' => self::mtHandleGood($goods),
            ];
        }
        return [
            'order_info' => $order_info,
            'good_list'  => $good_list,
            'extra_info' => array_merge(self::mtGetExtraInfo($order), $extra_info),
        ];
    }
    
    /**
     * 商品分组(袋子/篮子)
     *
     * @param array $order
     */
    private static function goodGroup($order)
    {
        $cart_ids = array_column($order['detail'], 'cart_id');
        $goods    = [];
        foreach ($cart_ids as $cart_id) {
            $goods[$cart_id] = array_filter($order['detail'], function ($v) use ($cart_id) {
                return $v['cart_id'] == $cart_id;
            });
        }
        return $goods;
    }
    
    private static function mtHandleGood($goods)
    {
        $list = [];
        foreach ($goods as $good) {
            $list[] = [
                'name'      => $good['food_name'],
                'quantity'  => $good['quantity'],
                'price'     => $good['price'],
                'total'     => $good['actual_price'] * $good['quantity'],
                'specsInfo' => self::handleSpescInfoContainSymbol($good['food_property']),
            ];
        }
        return $list;
    }
    
    /**
     * 处理额外信息
     *
     * @param array $order 订单信息
     * @return array
     */
    private static function mtGetExtraInfo($order)
    {
        $extra_info = [];
        // 商铺名称
        if (isset($order['poiName']) && $order['poiName']) {
            $extra_info['shop_name'] = $order['poiName'];
        }
        return $extra_info;
    }
}
