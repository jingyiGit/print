<?php

namespace JyPrint\TemplatePrint;

/**
 * 饿了么
 * 内容格式说明：http://docs.ijingyi.com/web/#/74/2910
 */
trait EleTo
{
    /**
     * 针对饿了么订单，自动转换打印数据
     *
     * @param array $order      订单信息
     * @param array $extra_info 额外信息
     * @return array|false
     */
    public static function transformationEle($order, $extra_info = [])
    {
        if (!self::isEleOrder($order)) {
            return false;
        }
        
        $order_info = [
            'orderId'                => $order['orderId'],
            'daySeq'                 => $order['daySn'],
            'Remark'                 => $order['description'],
            'peopleNumber'           => 0,
            'order_time'             => self::handleTime($order['createdAt']),
            'deliveryTime'           => self::handleTime($order['deliverTime']),
            'reserve_time'           => self::handleTime($order['deliverTime']),
            
            // 价格相关
            'originalPrice'          => $order['originalPrice'], // 原价
            'total'                  => $order['totalPrice'],    // 总价(实际收入)
            
            // 收货信息
            'customerName'           => $order['consignee'],
            'customerPhone'          => is_array($order['consigneePhones']) ? $order['consigneePhones'][0] : $order['consigneePhones'],
            'customerAddress'        => $order['deliveryPoiAddress'],
            'customerAddressDetails' => '',
            
            // 骑手信息
            'riderName'              => '',
            'riderPhone'             => '',
        ];
        
        // 商品列表
        $good_list = [];
        foreach ($order['groups'] as $group) {
            $good_list[] = [
                'name'  => $group['name'],
                'type'  => $group['type'],
                'items' => self::eleHandleGood($group['items']),
            ];
        }
        return [
            'order_info' => $order_info,
            'good_list'  => $good_list,
            'extra_info' => array_merge(self::eleGetExtraInfo($order), $extra_info),
        ];
    }
    
    private static function eleHandleGood($goods)
    {
        $list = [];
        foreach ($goods as $good) {
            // 非商品，是口味，直接跳过
            if ($good['foodType'] == 3) {
                continue;
            }
            
            $temp         = [
                'name'      => $good['name'],
                'quantity'  => $good['quantity'],
                'price'     => $good['price'],
                'total'     => $good['total'],
                'specsInfo' => self::eleHandleSpescInfo($good),
            ];
            $temp['name'] = self::handleGoodName($temp['name'], $temp['specsInfo']);
            $list[]       = $temp;
        }
        return $list;
    }
    
    /**
     * 获取规格/品味
     *
     * @param $good
     * @return string
     */
    private static function eleHandleSpescInfo($good)
    {
        $specs = [];
        // 属性
        foreach ($good['attributes'] ?? [] as $v) {
            $specs[] = $v['value'];
        }
        
        // 规格
        foreach ($good['newSpecs'] ?? [] as $v1) {
            $specs[] = $v1['value'];
        }
        
        // 商品是否有成分(品味)
        if (isset($good['ingredients']) && $good['ingredients']) {
            foreach ($good['ingredients'] as $v2) {
                $specs[] = $v2['name'];
            }
        }
        
        return self::handleSpescInfoContainSymbol(implode(',', $specs));
    }
    
    /**
     * 饿了么的订单商品标题，带了规格品味，要去除掉
     *
     * @param string $name      订单商品标题
     * @param string $specsInfo 规格品味
     */
    private static function handleGoodName($name, $specsInfo)
    {
        if (preg_match('#\[(.*?)\]#', $name, $res)) {
            $isReplace = false;
            if (strpos($res[1], '+')) {
                $temp = explode('+', $res[1]);
                foreach ($temp as $v) {
                    if (strpos($specsInfo, $v) !== false) {
                        $isReplace = true;
                    }
                }
                
            } else if (strpos($specsInfo, $res[1]) !== false) {
                $isReplace = true;
            }
            
            if ($isReplace) {
                $name = preg_replace('#\[.*?\]#', '', $name);
            }
        }
        return $name;
    }
    
    /**
     * 处理额外信息
     *
     * @param array $order 订单信息
     * @return array
     */
    private static function eleGetExtraInfo($order)
    {
        $extra_info = [];
        // 处理虚拟号码
        if (isset($order['phoneList']) && $order['phoneList']) {
            foreach ($order['phoneList'] as $number => $v) {
                $number++;
                $temp                                  = explode(',', $v);
                $phone                                 = $temp[0];
                $extension                             = $temp[1] ?? '';
                $extra_info['virtualNumber' . $number] = "虚拟号码{$number}: {$phone} 转 {$extension}";
            }
            $extra_info['virtualDescribe'] = '【如需联系顾客，请在呼叫主号码听到提示音后输入分机号，即可拨通顾客隐私号。】';
        }
        
        // 商铺名称
        if (isset($order['shopName']) && $order['shopName']) {
            $extra_info['shop_name'] = $order['shopName'];
        }
        return $extra_info;
    }
}
