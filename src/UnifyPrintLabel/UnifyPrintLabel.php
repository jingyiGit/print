<?php
// 统计打印，标签类

namespace JyPrint\UnifyPrintLabel;

class UnifyPrintLabel
{
    use Fun;
    
    public $printFormat = [];      // 打印格式
    protected string $printContent = '';
    protected $returnContent = false;
    protected $y = 10;
    protected $paperWidth = 40;    // 纸张宽度，mm
    protected $paperHeight = 30;   // 纸张高度，mm
    
    public function __construct($format)
    {
        $this->printFormat = $format;
        
        // dots倍数
        if ($this->printFormat['scale'] > 0) {
            $this->paperWidth  = $this->paperWidth * $this->printFormat['scale'];
            $this->paperHeight = $this->paperHeight * $this->printFormat['scale'];
        }
    }
    
    /**
     * 设置打印机设备内部的纸张大小
     *
     * @param int $width  宽度，如：40
     * @param int $height 高度，如：30
     * @return void
     */
    public function deviceSetPaperSize($width, $height)
    {
        $this->printContent = str_replace(['{width}', '{height}'], [
            $width,
            $height,
        ], $this->printFormat['set_paper_size']);
    }
    
    /**
     * 设置机器内部的打印方式
     *
     * @param int $direction 打印方向，1=正向出纸，0=反向出纸
     * @return void
     */
    public function deviceSetPrintDirection($direction = 1)
    {
        $this->push($direction == 1 ? $this->printFormat['print_direction_forward'] : $this->printFormat['print_direction_reverse']);
    }
    
    /**
     * 设置纸张大小
     *
     * @param int $width  宽度，如：40
     * @param int $height 高度，如：30
     * @param int $margin 边距
     * @return void
     */
    public function setPaperSize($width, $height, $margin = false)
    {
        // dots倍数
        if ($this->printFormat['scale'] > 0) {
            $this->paperWidth  = $width * $this->printFormat['scale'];
            $this->paperHeight = $height * $this->printFormat['scale'];
        }
        if ($margin !== false && $margin >= 0) {
            $this->printFormat['margin'] = $margin;
            $this->y                     = $margin;
        }
        
        // 佳博的特殊处理
        if ($this->printFormat['owner'] == 'Poscom') {
            // 设置纸张大小
            $this->deviceSetPaperSize($width, $height);
        }
    }
    
    /**
     * h1大标题
     *
     * @param string $value
     * @param string $align 对齐方式，left，center，right
     * @return void
     */
    public function h1($value, $align = 'left', $height = true, $width = true)
    {
        $this->push($this->addText($value, $height, $width, $align));
    }
    
    /**
     * h1大标题
     *
     * @param string $value
     * @param string $align 对齐方式，left，center，right
     * @return void
     */
    public function h2($value, $align = 'left', $height = true, $width = false)
    {
        $this->push($this->addText($value, $height, $width, $align));
    }
    
    /**
     * 常规文本
     *
     * @param string $value
     * @param string $align 对齐方式，left，center，right
     * @return string
     */
    public function text($value, $height = false, $width = false, $align = 'left')
    {
        $temp = $this->addText($value, $height, $width, $align);
        $this->push($temp);
        return $temp;
    }
    
    /**
     * 上下居中，左右居中，支持换行显示
     *
     * @param string $value  文本
     * @param bool   $height 是否加高
     * @param bool   $width  是否加宽
     * @return void
     */
    public function text_center($value, $height = false, $width = false)
    {
        // 按纸张宽度，自动分割行，并返回行数和每行的文本宽度
        $rows = $this->splitRow($value, $height, $width);
        
        $temp_width  = $width ? 2 : 1;
        $temp_height = $height ? 2 : 1;
        
        // 字体高度
        $font_height = $height ? $this->printFormat['h1_font_height'] : $this->printFormat['normal_font_height'];
        $baseY       = ($this->paperHeight / 2) - intval($font_height * count($rows) / 2);
        foreach ($rows as $index => $v) {
            $x = ($this->paperWidth - $v['width']) / 2;
            $y = $baseY + ($font_height * $index);
            $this->push($this->getBaseText($v['text'], $x, $y, $temp_height, $temp_width));
        }
    }
    
    /**
     * 文本，9宫格格
     *
     * @param string $value
     * @param int    $position 1上左，2上右，3中左，4中右，5下左，6下右
     * @param bool   $height   是否加高
     * @param bool   $width    是否加宽
     * @return void
     */
    public function text_position($value, $position = 1, $height = false, $width = false)
    {
        // 按纸张宽度，自动分割行，并返回行数和每行的文本宽度
        $info        = first($this->splitRow($value, $height, $width));
        $temp_width  = $width ? 2 : 1;
        $temp_height = $height ? 2 : 1;
        
        $right = $this->paperWidth - $info['width'] - 20;
        
        // 上左
        if ($position == 1) {
            $x = $y = 20;
            
            // 上中
        } else if ($position == 2) {
            $x = ($this->paperWidth / 2) - ($info['width'] / 2);
            $y = 20;
            
            // 上右
        } else if ($position == 3) {
            $x = $right;
            $y = 20;
            
            // 中左
        } else if ($position == 4) {
            $x = 20;
            $y = ($this->paperHeight / 2) - ($info['height'] / 2);
            
            // 中中
        } else if ($position == 5) {
            $x = ($this->paperWidth / 2) - ($info['width'] / 2);
            $y = ($this->paperHeight / 2) - ($info['height'] / 2);
            
            // 中右
        } else if ($position == 6) {
            $x = $right;
            $y = ($this->paperHeight / 2) - ($info['height'] / 2);
            
            // 下左
        } else if ($position == 7) {
            $x = 20;
            $y = $this->paperHeight - $info['height'] - 20;
            
            // 下中
        } else if ($position == 8) {
            $x = ($this->paperWidth / 2) - ($info['width'] / 2);
            $y = $this->paperHeight - $info['height'] - 20;
            
            // 下右
        } else if ($position == 9) {
            $x = $right;
            $y = $this->paperHeight - $info['height'] - 20;
        }
        
        $this->push($this->getBaseText($value, $x, $y, $temp_height, $temp_width));
    }
    
    /**
     * 添加文本，自动处理x和y坐标
     *
     * @param string $value  文本
     * @param bool   $height 是否加高
     * @param bool   $width  是否加宽
     * @param string $align  对齐方式，left，center，right
     * @return string
     */
    private function addText($value, $height = false, $width = false, $align = 'left')
    {
        $temp_width  = $width ? 2 : 1;
        $temp_height = $height ? 2 : 1;
        $rows        = $this->splitRow($value, $height, $width);
        
        $str = '';
        foreach ($rows as $v) {
            // 居左
            if ($align == 'left') {
                $x = $this->printFormat['margin'];
                
                // 居中
            } else if ($align == 'center') {
                $x = intval(($this->paperWidth - $v['width']) / 2);
                
                // 居右
            } else {
                $x = $this->paperWidth - $v['width'] - $this->printFormat['margin'];
            }
            
            $str .= $this->getBaseText($v['text'], $x, $this->y, $temp_height, $temp_width);
            
            // y 坐标累加
            $this->y += ($height ? $this->printFormat['h1_font_height'] : $this->printFormat['normal_font_height']) + $this->printFormat['row_spacing'];
        }
        return $str;
    }
    
    /**
     * 增加行距
     *
     * @param int $row_spacing 行距
     * @return void
     */
    public function br($row_spacing = 10)
    {
        $this->y += $row_spacing;
    }
    
    public function splitLine($line = '-')
    {
        $length = intval(($this->paperWidth - $this->printFormat['margin'] * 2) / ($this->printFormat['normal_font_width'] / 2)) - 2;
        $value  = str_pad('', $length, $line);
        $this->text($value, false, false, 'center');
    }
    
    /**
     * 获取内容
     *
     * @return string
     */
    public function getPrintContent()
    {
        return $this->printContent . $this->printFormat['after'] ?: '';
    }
    
    /**
     * 清空内容
     *
     * @return void
     */
    public function clear()
    {
        $this->printContent = '';
        $this->y            = $this->printFormat['margin'];
    }
    
    private function push($value)
    {
        $this->printContent .= $value;
    }
    
    private function getBaseText($value, $x = 10, $y = 10, $h = 1, $w = 1, $font = '12', $r = 0)
    {
        $str = str_replace('{value}', $value, $this->printFormat['basics_text']);
        $str = str_replace('{font}', $font, $str);
        $str = str_replace('{r}', $r, $str);
        $str = str_replace('{x}', $x, $str);
        $str = str_replace('{y}', $y, $str);
        $str = str_replace('{h}', $h, $str);
        return str_replace('{w}', $w, $str);
    }
}
