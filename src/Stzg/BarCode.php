<?php

namespace JyPrint\Stzg;

use JyUtils\Str\Str;

trait BarCode
{
    /**
     * 初始化统一打印
     *
     * @param array $print_data
     */
    public function initPrintContent($print_data = [])
    {
        if (!$print_data) {
            return [];
        }
        // 订单时间处理，重要
        $order_time = $print_data['order_info']['order_time'];
        $orderedAt  = $this->handleTime($order_time);
        
        // 取货时间
        if ($print_data['order_info']['pickup_time']) {
            $pickup_time = $this->handleTime($print_data['order_info']['pickup_time']);
        } else {
            $pickup_time = $orderedAt;
        }
        
        // 预约时间
        if ($print_data['order_info']['reserve_time']) {
            $reserve_time = $this->handleTime($print_data['order_info']['reserve_time']);
        } else {
            $reserve_time = $orderedAt;
        }
        
        // 收货地址处理
        $address = $print_data['order_info']['customerAddressDetails'] ?: $print_data['order_info']['customerAddress'];
        
        // 订单号(订单码)处理
        $daySeq = $print_data['order_info']['daySeq'] ?: $print_data['order_info']['orderId'];
        
        // 由于水獭是由创建订单来打印的，订单id不能相同
        $temp    = substr(TIMESTAMP, -2) . Str::random(2);
        $orderId = $print_data['order_info']['orderId'] . "({$temp})";
        
        return [
            'externalIdentifiers' => [
                'id'         => $orderId,
                'friendlyId' => $daySeq,
            ],
            'items'               => $this->getGoodList($print_data['good_list']),
            'orderedAt'           => $orderedAt,                           // 下单时间
            'currencyCode'        => 'CNY',                                // 货币类型
            'customerNote'        => $print_data['order_info']['Remark'],  // 订单备注
            'status'              => 'NEW_ORDER',                          // 订单状态
            
            // 顾客信息
            'customer'            => [
                "name"                => $print_data['order_info']['customerName'],
                "phone"               => $print_data['order_info']['customerPhone'],
                "email"               => "",
                'personalIdentifiers' => [
                    'taxIdentificationNumber' => '01234567890',
                ],
            ],
            
            // 配送信息
            'deliveryInfo'        => [
                // 骑手信息
                'courier'           => [
                    "name"                => $print_data['order_info']['riderName'],
                    "phone"               => $print_data['order_info']['riderPhone'],
                    "email"               => "",
                    'personalIdentifiers' => [
                        'taxIdentificationNumber' => '01234567890',
                    ],
                ],
                'destination'       => [
                    "postalCode"   => "",
                    "city"         => "",
                    "state"        => "",
                    "countryCode"  => "",
                    "addressLines" => [$address],
                    'location'     => [
                        "latitude"  => 38.8977,
                        "longitude" => 77.0365,
                    ],
                ],
                "licensePlate"      => "ABC 123",
                "makeModel"         => "Honda CR-V",
                "note"              => "Gate code 123",
                'lastKnownLocation' => [
                    "latitude"  => 38.8977,
                    "longitude" => 77.0365,
                ],
            ],
            
            // 订单信息
            'orderTotal'          => [
                "subtotal"    => 0,
                "discount"    => 0,
                "tax"         => 1.1,
                "tip"         => 2,
                "deliveryFee" => 0,
                "total"       => $print_data['order_info']['total'],
                "couponCode"  => "VWXYZ98765",
            ],
            
            'customerPayments' => [
                [
                    "value"            => 2,
                    "processingStatus" => "PROCESSED",
                    "paymentMethod"    => "UNKNOWN",
                ],
            ],
            
            'fulfillmentInfo' => [
                'pickupTime'      => $pickup_time,    // 取货时间
                'deliveryTime'    => $reserve_time,   // 送达时间
                "fulfillmentMode" => "DELIVERY",
                "schedulingType"  => "ASAP",
                "courierStatus"   => "COURIER_ASSIGNED",
                "tableIdentifier" => $print_data['extra_info']['tableNumber'] ?: '',
            ],
        ];
    }
    
    /**
     * 处理时间
     *
     * @param string|int $datetime
     * @return string
     */
    private function handleTime($datetime)
    {
        if (strlen(trim($datetime)) == 10) {
            $datetime  = $datetime - 8 * 3600;
            $orderedAt = date('Y-m-d', $datetime) . 'T' . date('H:i:s', $datetime) . 'Z';
            
        } else {
            // 11-30 16:27、12-01 12:25:41，针对这种时间格式的，智能补全
            if ((strlen(trim($datetime)) == 11 || strlen(trim($datetime)) == 14) && strpos($datetime, '-') !== false) {
                $order_time = strtotime(date('Y') . '-' . $datetime);
            } else {
                $order_time = strtotime($datetime);
            }
            
            $order_time = $order_time < TIMESTAMP ? TIMESTAMP : $order_time;
            $order_time = $order_time - 8 * 3600;
            $orderedAt  = date('Y-m-d', $order_time) . 'T' . date('H:i:s', $order_time) . 'Z';
        }
        return $orderedAt;
    }
    
    private function getGoodList($good_list)
    {
        $list        = [];
        $basketCount = count($good_list);
        foreach ($good_list as $basket) {
            if ($basketCount > 1) {
                $list[] = [
                    "id"           => Str::uuid(),
                    "name"         => '- - - ' . $basket['name'],
                    "quantity"     => 1,
                    "note"         => "",
                    "categoryId"   => "303de078-870d-4349-928b-946869d4d69b",
                    "categoryName" => "Burgers",
                    "price"        => 0,
                ];
            }
            foreach ($basket['items'] as $v) {
                if (!$v['quantity'] || $v['quantity'] < 0) {
                    $v['quantity'] = 1;
                }
                
                $temp = [
                    "id"           => Str::uuid(),
                    "name"         => $v['name'],
                    "quantity"     => $v['quantity'],
                    "note"         => "",
                    "categoryId"   => "303de078-870d-4349-928b-946869d4d69b",
                    "categoryName" => "Burgers",
                    "price"        => $v['quantity'] > 1 ? ($v['total'] / $v['quantity']) : $v['total'],
                ];
                
                // 规格
                if (isset($v['specsInfo']) && $v['specsInfo']) {
                    $temp['modifiers'][] = [
                        "id"        => "d7a21692-9195-43aa-a58f-5395bba8a804",
                        "name"      => $v['specsInfo'],
                        "quantity"  => 1,
                        "price"     => 0,
                        "groupName" => "",
                        "groupId"   => "fb52b138-7ac4-42c1-bfd8-664d57113a41",
                    ];
                }
                $list[] = $temp;
            }
        }
        return $list;
    }
}
