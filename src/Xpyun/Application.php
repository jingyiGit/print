<?php
// 芯烨云
// https://www.xpyun.net/open/index.html

namespace JyPrint\Xpyun;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

/**
 * Class Application.
 */
class Application extends Response
{
    use BarCode;
    use BarCodeLabel;
    
    private $config;
    private $requestUrl = 'https://open.xpyun.net/api/openapi';
    protected $printName = '芯烨云';
    protected $errorNum = [
        '-1'   => '请求头错误',
        '-2'   => '参数不合法',
        '-3'   => '参数签名失败',
        '-4'   => '	用户未注册',
        '1001' => '打印机编号和用户不匹配',
        '1002' => '打印机未注册',
        '1003' => '打印机不在线',
        '1004' => '添加订单失败',
        '1005' => '未找到订单信息',
        '1006' => '订单日期格式或大小不正确  ',
        '1007' => '打印内容不能超过12K   ',
        '1008' => '用户修改打印机记录失败',
        '1009' => '用户添加打印机时，打印机编号或名称不能为空',
        '1010' => '打印机设备编号无效',
        '1011' => '打印机已存在，若当前开放平台无法查询到打印机信息，请联系售后技术支持人员核实',
        '1012' => '添加打印设备失败，请稍后再试或联系售后技术支持人员',
    ];
    
    public function __construct(array $config = [])
    {
        $this->config = $config;
        $this->setError('');
    }
    
    /**
     * 添加打印机
     */
    public function add($param)
    {
        unset($param['key']);
        $params = $this->getCommonParam(['items' => [$param]]);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/addPrinters', $params);
        if (isset($res['code']) && $res['code'] == 0) {
            if (isset($res['data']['fail']) && count($res['data']['fail']) > 0) {
                // 错误码查看 https://www.xpyun.net/open/index.html
                $error_num = str_replace(':', '', strstr($res['data']['failMsg'][0], ':'));
                // 打印机已存在
                if ($error_num == 1011) {
                    return true;
                } else if (isset($this->errorNum[$error_num])) {
                    $this->setError($this->errorNum[$error_num]);
                    return false;
                } else {
                    $this->setError($res);
                    return false;
                }
            }
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 删除打印机
     */
    public function del($param)
    {
        $params = $this->getCommonParam(['snlist' => $param]);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/delPrinters', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 清空待打印队列
     */
    public function clear($sn)
    {
        $params = $this->getCommonParam(['sn' => $sn]);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/delPrinterQueue', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 获取打印机状态
     * 0=离线，1=在线，2=缺纸
     * https://www.kancloud.cn/fage/us_api/1342979
     */
    public function getStatus($param)
    {
        $param  = $this->onlyfields($param, ['sn']);
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/queryPrinterStatus', $params);
        if ($res['code'] == 0) {
            return $res['data'];
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 设置打印机语音类型
     *
     * @param array $param
     * @return bool|false
     */
    public function setVoiceType($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/setVoiceType', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印小票
     */
    public function printTicket($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/print', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 打印标签
     */
    public function printLabel($param)
    {
        $params = $this->getCommonParam($param);
        $res    = Http::httpPostJson($this->requestUrl . '/xprinter/printLabel', $params);
        if ($res['code'] == 0) {
            return true;
        }
        $this->setError($res);
        return false;
    }
    
    /**
     * 取公共参数
     */
    private function getCommonParam($params)
    {
        $time         = time();
        $data         = array_merge([
            'user'      => $this->config['user'],
            'timestamp' => $time,
            'debug'     => 1,
        ], $params);
        $data['sign'] = $this->getSign($time);
        return $data;
    }
    
    /**
     * 生成签名
     */
    private function getSign($time)
    {
        return SHA1($this->config['user'] . $this->config['secret'] . $time);
    }
}
