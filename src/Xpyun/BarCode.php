<?php

namespace JyPrint\Xpyun;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Xpyun',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => '<BR>',
            'br_inside'   => true,
            'lineAndBr'   => false,
            'cut'         => '<CUT>',
            'barCode1'    => '<C><BARCODE>{value}</BARCODE><BR></C>',
            'qrCode'      => '<C><QR>{value}</QR><BR></C>',
            'left'        => '<L>{value}<BR></L>',
            'center'      => '<C>{value}<BR></C>',
            'right'       => '<R>{value}<BR></R>',
            'font_h1'     => '<B>{value}</B>',
            'font_h2'     => '<CB>{value}</CB>',
            'font_height' => '<HB>{value}</HB>',
            'font_width'  => '<WB>{value}</WB>',
            'font_bold'   => '<BOLD>{value}</BOLD>',
            'font_big'    => '<B>{value}</B>',
            'leftRight'   => false, // 左右对齐
            'row2col'     => false, // 1行2列
            'row3col'     => false, // 1行3列
            'row4col'     => false, // 1行4列
            // 排序，从[外]到[里]的顺序，越外面，放在越前面
            'sort'        => [
                'CB',
                'R',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
