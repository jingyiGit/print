<?php

namespace JyPrint\Ylyun;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Ylyun',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => "\r\n",
            'lineAndBr'   => false,
            'cut'         => '<MK2></MK2>',
            'cashBox'     => '<PLUGIN>',
            // 钱箱/声光报警器
            'barCode1'    => '<center><BR>{value}</BR></center>',
            'barCode2'    => '<center><BR3>{value}</BR3></center>',
            'qrCode'      => '<center><QR>{value}</QR></center>',
            'left'        => "<LR>{value}</LR>",
            'center'      => "<center>{value}</center>",
            'right'       => "<right>{value}</right>",
            'font_h1'     => '<FS2>{value}</FS2>',
            'font_h2'     => '<FS2>{value}</FS2>',
            'font_height' => '<FH2>{value}</FH2>',
            'font_width'  => '<FW2>{value}</FW2>',
            'font_bold'   => '<FB>{value}</FB>',
            'font_big'    => '<FS2>{value}</FS2>',
            'leftRight'   => false,
            // 左右对齐
            'row2col'     => '<table><tr><td>{content1}</td><td>{content2}</td></tr></table>',
            // 1行2列
            'row3col'     => '<table><tr><td>{content1}</td><td>{content2}</td><td>{content3}</td></tr></table>',
            // 1行3列
            'row4col'     => '<table><tr><td>{content1}</td><td>{content2}</td><td>{content3}</td><td>{content4}</td></tr></table>',
            // 1行4列
            'sort'        => [
                'FB',
                'right',
                'center',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
