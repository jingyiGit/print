<?php
// 易联云
// http://doc2.10ss.net/371769

namespace JyPrint\Ylyun;

use JyPrint\Kernel\Http;
use JyPrint\Kernel\Response;

class Application extends Response
{
  use Auth;
  use BarCode;
  
  private $config;
  private $requestUrl = 'https://open-api.10ss.net';
  
  public function __construct(array $config = [])
  {
    $this->config = $config;
    $this->setError('');
  }
  
  /**
   * 清空打印机队列
   * 0离线 1在线 2缺纸
   */
  public function clear($param)
  {
    $param['machine_code'] = $param['sn'];
    unset($param['sn']);
    $data = $this->getCommonParam($param);
    $res  = Http::httpPost($this->requestUrl . '/printer/cancelall', $data);
    if ($res['error'] == 0) {
      return true;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 设置设备音量
   *
   * @param array $param
   * @return false|mixed
   */
  public function setVolume($param)
  {
    $param['machine_code']  = $param['sn'];
    $param['voice']         = $param['sound'];
    $param['response_type'] = $param['response_type'] ?: 'horn';
    unset($param['sn'], $param['sound']);
    $data = $this->getCommonParam($param);
    $res  = Http::httpPost($this->requestUrl . '/printer/setsound', $data);
    if ($res['error'] == 0) {
      return true;
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 取打印机状态
   * 0离线 1在线 2缺纸
   */
  public function getStatus($param)
  {
    $param                 = $this->onlyfields($param, ['sn', 'key']);
    $param['machine_code'] = $param['sn'];
    $param['access_token'] = $param['key'];
    unset($param['sn'], $param['key']);
    $data = $this->getCommonParam($param);
    $res  = Http::httpPost($this->requestUrl . '/printer/getprintstatus', $data);
    if ($res['error'] == 0 && isset($res['body'])) {
      return (int)$res['body']['state'];
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 取机型打印宽度接口
   */
  public function getInfo($param)
  {
    $param['machine_code'] = $param['sn'];
    unset($param['sn']);
    $data = $this->getCommonParam($param);
    $res  = Http::httpPost($this->requestUrl . '/printer/printinfo', $data);
    if ($res['error'] == 0 && isset($res['body'])) {
      return $res['body'];
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 打印小票
   */
  public function printTicket($param)
  {
    $param['machine_code'] = $param['sn'];
    $param['origin_id']    = isset($data['order_id']) ? $param['order_id'] : $this->uuid();
    unset($param['sn']);
    if (isset($param['order_id'])) {
      unset($param['order_id']);
    }
    $data = $this->getCommonParam($param);
    $res  = Http::httpPost($this->requestUrl . '/print/index', $data);
    if ($res['error'] == 0) {
      return $res['body'];
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 取公共参数
   */
  private function getCommonParam($params)
  {
    $time         = time();
    $data         = array_merge([
      'client_id' => $this->config['client_id'],
      'id'        => $this->uuid(),
      'timestamp' => $time,
    ], $params);
    $data['sign'] = $this->getSign($time);
    return $data;
  }
  
  /**
   * 生成签名
   */
  private function getSign($time)
  {
    return md5($this->config['client_id'] . $time . $this->config['secret']);
  }
}
