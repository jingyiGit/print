<?php

namespace JyPrint\Ylyun;

use JyPrint\Kernel\Http;

trait Auth
{
  /**
   * 网页授权模式
   *
   * @param array $param
   * @param null  $fn
   */
  public function webAuth($param, $fn = null)
  {
    if (!isset($_GET['code'])) {
      $url = $this->createAuthUrl($param);
      header('Location: ' . $url);
      exit();
    } else {
      $this->getAccessTokenByCode(trim($_GET['code']), $fn);
    }
  }
  
  public function createAuthUrl($param)
  {
    $data = [
      'client_id'     => $this->config['client_id'],
      'response_type' => 'code',
      'redirect_uri'  => $param['redirect_uri'],
      'state'         => $param['state'],
    ];
    return 'https://open-api.10ss.net/oauth/authorize?' . http_build_query($data);
  }
  
  public function getAccessTokenByCode($code, $fn = null)
  {
    $time         = time();
    $data         = [
      'client_id'  => $this->config['client_id'],
      'grant_type' => 'authorization_code',
      'code'       => $code,
      'scope'      => 'all',
      'timestamp'  => $time,
      'id'         => $this->uuid(),
    ];
    $data['sign'] = $this->getSign($time);
    $res          = Http::httpPost('https://open-api.10ss.net/oauth/oauth', $data);
    if ($fn) {
      $fn($res);
    } else {
      return $res;
    }
  }
  
  /**
   * 扫码授权
   *
   * @param array $param
   */
  public function scanAuth($param)
  {
    $time = time();
    $data = [
      'client_id'    => $this->config['client_id'],
      'machine_code' => $param['sn'],
      'scope'        => 'all',
      'id'           => $this->uuid(),
      'timestamp'    => $time,
    ];
    if (isset($param['qr_key'])) {
      $data['qr_key'] = $param['qr_key'];
    }
    if (isset($param['msign'])) {
      $data['msign'] = $param['msign'];
    }
    $data['sign'] = $this->getSign($time);
    $res          = Http::httpPost('https://open-api.10ss.net/oauth/scancodemodel', $data);
    if (isset($res['error']) && $res['error'] == 0) {
      return $res['body'];
    }
    $this->setError($res . '，请检查终端密钥是否输入有误');
    return false;
  }
  
  /**
   * 刷新AccessToken
   *
   * @param string $refresh_token
   * @return false|mixed
   */
  public function refreshToken($refresh_token)
  {
    $time         = time();
    $data         = [
      'client_id'     => $this->config['client_id'],
      'grant_type'    => 'refresh_token',
      'scope'         => 'all',
      'refresh_token' => $refresh_token,
      'id'            => $this->uuid(),
      'timestamp'     => $time,
    ];
    $data['sign'] = $this->getSign($time);
    $res          = Http::httpPost('https://open-api.10ss.net/oauth/oauth', $data);
    if ($res['error'] == 0) {
      return $res['body'];
    }
    $this->setError($res);
    return false;
  }
  
  /**
   * 删除授权
   */
  public function del($sn, $access_token)
  {
    $time         = time();
    $data         = [
      'client_id'    => $this->config['client_id'],
      'machine_code' => $sn,
      'access_token' => $access_token,
      'id'           => $this->uuid(),
      'timestamp'    => $time,
    ];
    $data['sign'] = $this->getSign($time);
    $res          = Http::httpPost($this->requestUrl . '/printer/deleteprinter', $data);
    if (isset($res['error']) && $res['error'] == 0) {
      return true;
    }
    $this->setError($res);
    return false;
  }
}
