<?php

namespace JyPrint\Ylyun;

trait Audio
{
  /**
   * Ai语音播报
   * 仅支持型号中有字母A的机器！例如K4-WA、K4-GAD、K4-WGEAD、k6-WEAD等等
   *
   * @param string $content    为语音内容支持100个以内的字符，超出将无法播报．
   * @param int    $volume     音量标识(1 -9)若不填默认为5，
   * @param int    $categories 声音类别(0普通女声，1普通男声，3合成男声，4合成女声)
   * @return string
   */
  public function audio($content, $volume = 5, $categories = 0)
  {
    return "<audio>{$content},{$volume},{$categories}</audio>";
  }
}
