<?php

namespace JyPrint\Zhongwuyun;

use JyPrint\UnifyPrint\UnifyPrint;

trait BarCode
{
    /**
     * 初始化统一打印
     */
    public function initPrintContent($paper = [])
    {
        $config = [
            'owner'       => 'Zhongwuyun',
            'model'       => 'label',
            'labelName'   => false,
            'text'        => false,
            'br'          => "<RN>",
            'line'        => "~",
            'cut'         => false,
            'cashBox'     => '<PLUGIN>', // 钱箱/声光报警器
            'barCode1'    => '<C><BR>{value}</BR></C>',
            'qrCode'      => '<C><QR>{value}</QR></C>',
            'left'        => "{value}<RN>",
            'center'      => "<C>{value}</C>",
            'right'       => "<R>{value}</R>",
            'font_h1'     => '<S2>{value}</S2>',
            'font_h2'     => '<S2>{value}</S2>',
            'font_height' => '<H2>{value}</H2>',
            'font_width'  => '<W2>{value}</W2>',
            'font_bold'   => '<B1>{value}</B1>',
            'font_big'    => '<S2>{value}</S2>',
            'leftRight'   => false,                                                                                   // 左右对齐
            'row2col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD></TR>',                                       // 1行2列
            'row3col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD><TD>{content3}</TD></TR>',                    // 1行3列
            'row4col'     => '<TR><TD>{content1}</TD><TD>{content2}</TD><TD>{content3}</TD><TD>{content4}</TD></TR>', // 1行4列
            'sort'        => [
                'B1',
                'C',
                'R',
                'S1',
                'S2',
            ],
        ];
        return new UnifyPrint($config, $paper);
    }
}
